package at.fhv.hadoop.examples.avro;

import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.junit.Test;

import java.io.*;
import java.nio.file.Files;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class AvroTest {
    @Test
    public void inMemorySerializationAndDeserialization() throws Exception {

        //Create the parser for the schema
        Schema.Parser parser = new Schema.Parser();

        //Create schema from .avsc file
        Schema schema = parser.parse(
                getClass().getResourceAsStream(TestContext.INSTANCE.stringPairSchemaFile())
        );

        //Create the test record
        GenericRecord datum = new GenericData.Record(schema);
        datum.put("left", "L");
        datum.put("right", "R");
        //datum.put("t", 1);

        //Create the outputStream
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        //Create the writer for a record
        DatumWriter<GenericRecord> writer = new GenericDatumWriter<>(schema);

        //Create the encoder
        Encoder encoder = EncoderFactory.get().binaryEncoder(out, null);

        //Write the record
        writer.write(datum, encoder);
        encoder.flush();

        DatumReader<GenericRecord> reader = new GenericDatumReader<>(schema);
        Decoder decoder = DecoderFactory.get().binaryDecoder(out.toByteArray(), null);

        GenericRecord result = reader.read(null, decoder);

        //System.out.println(result.get("t").getClass());


        assertThat(result.get("left").toString(), is("L"));
        assertThat(result.get("right").toString(), is("R"));

        //Close stream
        out.close();
    }

    @Test
    public void serializeToFile() throws Exception {

        //Create the parser for the schema
        Schema.Parser parser = new Schema.Parser();

        //Create schema from .avsc file
        Schema schema = parser.parse(getClass().getResourceAsStream(TestContext.INSTANCE.stringPairSchemaFile()));

        //Create the test record
        GenericRecord datum = new GenericData.Record(schema);
        datum.put("left", "L");
        datum.put("right", "R");

        try (FileOutputStream fos = new FileOutputStream(TestContext.INSTANCE.testFile())) {

            //Create the writer for a record
            DatumWriter<GenericRecord> writer = new GenericDatumWriter<>(schema);

            //Create the encoder
            Encoder encoder = EncoderFactory.get().binaryEncoder(fos, null);

            //Write the record
            writer.write(datum, encoder);
            encoder.flush();
        }
    }

    @Test
    public void deserializeFromFile() throws Exception {

        //Create the parser for the schema
        Schema.Parser parser = new Schema.Parser();

        //Create schema from .avsc file
        Schema schema = parser.parse(getClass().getResourceAsStream(TestContext.INSTANCE.stringPairSchemaFile()));

        //Create the reader
        DatumReader<GenericRecord> reader = new GenericDatumReader<>(schema);

        //Create the decoder
        Decoder decoder = DecoderFactory.get().binaryDecoder(
                Files.readAllBytes(TestContext.INSTANCE.testFile().toPath()), null);

        //Read the record
        GenericRecord result = reader.read(null, decoder);

        assertThat(result.get("left").toString(), is("L"));
        assertThat(result.get("right").toString(), is("R"));
    }

    @Test
    public void specificStringPairSerializationAndDeserialization() throws  Exception {

        //Create the record
        StringPair datum = new StringPair();
        datum.setLeft("L");
        datum.setRight("R");

        //Create the outputStream
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        //Create the writer for a record
        DatumWriter<StringPair> writer = new SpecificDatumWriter<>(StringPair.class);

        //Create the encoder
        Encoder encoder = EncoderFactory.get().binaryEncoder(out, null);

        //Write the record
        writer.write(datum, encoder);
        encoder.flush();

        //Close the stream
        out.close();

        DatumReader<StringPair> reader = new SpecificDatumReader<>(StringPair.class);
        Decoder decoder = DecoderFactory.get().binaryDecoder(out.toByteArray(), null);

        StringPair result = reader.read(null, decoder);

        assertThat(result.getLeft(), is("L"));
        assertThat(result.getRight(), is("R"));
    }

    @Test
    public void serializeToFileSpecific() throws Exception {

        //Create the record
        StringPair datum = new StringPair();
        datum.setLeft("L");
        datum.setRight("R");

        try (FileOutputStream fos = new FileOutputStream(TestContext.INSTANCE.testFile1())) {

            //Create the writer for a record
            DatumWriter<StringPair> writer = new SpecificDatumWriter<>(StringPair.class);

            //Create the encoder
            Encoder encoder = EncoderFactory.get().binaryEncoder(fos, null);

            //Write the record
            writer.write(datum, encoder);
            encoder.flush();
        }
    }

    @Test
    public void serializeToJsonSpecific() throws Exception {

        //Create the record
        StringPair datum = new StringPair();
        datum.setLeft("L");
        datum.setRight("R");

        //Create the parser for the schema
        Schema.Parser parser = new Schema.Parser();

        //Create schema from .avsc file
        Schema schema = parser.parse(getClass().getResourceAsStream(TestContext.INSTANCE.stringPairSchemaFile()));

        try (FileOutputStream fos = new FileOutputStream(TestContext.INSTANCE.testFile3())) {

            //Create the writer for a record
            DatumWriter<StringPair> writer = new SpecificDatumWriter<>(StringPair.class);

            //Create the encoder
            Encoder encoder = EncoderFactory.get().jsonEncoder(schema, fos);

            //Write the record
            writer.write(datum, encoder);
            encoder.flush();
        }
    }

    @Test
    public void deserializeFromFileSpecific() throws Exception {

        //Create the reader
        DatumReader<StringPair> reader = new SpecificDatumReader<>(StringPair.class);

        //Create the decoder
        Decoder decoder = DecoderFactory.get().binaryDecoder(Files.readAllBytes(TestContext.INSTANCE.testFile1().toPath()), null);

        //Read the record
        StringPair result = reader.read(null, decoder);

        assertThat(result.getLeft(), is("L"));
        assertThat(result.getRight(), is("R"));
    }

    @Test
    public void serializeToFile1() throws Exception {

        //Create the record
        StringPair datum = new StringPair();
        datum.setLeft("L");
        datum.setRight("R");

        //Create the writer for a record
        DatumWriter<StringPair> writer = new SpecificDatumWriter<>(StringPair.class);

        //Create the parser for the schema
        Schema.Parser parser = new Schema.Parser();

        //Create schema from .avsc file
        Schema schema = parser.parse(getClass().getResourceAsStream(TestContext.INSTANCE.stringPairSchemaFile()));

        try (DataFileWriter<StringPair> dataFileWriter = new DataFileWriter<>(writer)) {
            dataFileWriter.create(schema,TestContext.INSTANCE.testFile2());
            dataFileWriter.append(datum);
        }
    }

    @Test
    public void deserializeFromFile1() throws Exception{
        //Create the reader for a record
        DatumReader<StringPair> reader = new SpecificDatumReader<>(StringPair.class);

        try (DataFileReader<StringPair> dataFileReader = new DataFileReader<>(TestContext.INSTANCE.testFile2(), reader)) {

            StringPair record = null;
            while (dataFileReader.hasNext()) {
                record = dataFileReader.next(record);

                assertThat(record.getLeft(), is("L"));
                assertThat(record.getRight(), is("R"));
                assertThat(dataFileReader.hasNext(), is(false));
            }
        }
    }

    @Test
    public void deserializeExtendedFromFile() throws Exception {

        //Create the parser for the schema
        Schema.Parser parser = new Schema.Parser();

        //Create schema from .avsc file
        Schema oldSchema = parser.parse(getClass().getResourceAsStream(TestContext.INSTANCE.stringPairSchemaFile()));

        //Create the parser for the schema
        Schema.Parser parser1 = new Schema.Parser();

        //Create schema from .avsc file
        Schema newSchema = parser1.parse(getClass().getResourceAsStream(TestContext.INSTANCE.extendedStringPairSchemaFile()));
        //Schema newSchema = parser1.parse(getClass().getResourceAsStream(TestContext.INSTANCE.extendedStringPairSchemaFile1()));

        //Create the reader for a record
        DatumReader<GenericRecord> reader = new GenericDatumReader<>(oldSchema, newSchema);

        try (DataFileReader<GenericRecord> dataFileReader = new DataFileReader<>(TestContext.INSTANCE.testFile2(), reader)) {

            GenericRecord record = null;
            while (dataFileReader.hasNext()) {
                record = dataFileReader.next(record);

                assertThat(record.get("left").toString(), is("L"));
                assertThat(record.get("right").toString(), is("R"));
                assertThat(record.get("description").toString(), is(""));
                assertThat(dataFileReader.hasNext(), is(false));
            }
        }
    }

    @Test
    public void deserializeExtendedFromFile1() throws Exception {

        //Create the parser for the schema
        Schema.Parser parser = new Schema.Parser();

        //Create schema from .avsc file
        Schema oldSchema = parser.parse(getClass().getResourceAsStream(TestContext.INSTANCE.stringPairSchemaFile()));

        //Create the parser for the schema
        Schema.Parser parser1 = new Schema.Parser();

        //Create schema from .avsc file
        Schema newSchema = parser1.parse(getClass().getResourceAsStream(TestContext.INSTANCE.extendedStringPairSchemaFile1()));

        //Create the reader for a record
        DatumReader<GenericRecord> reader = new GenericDatumReader<>(oldSchema, newSchema);

        try (DataFileReader<GenericRecord> dataFileReader = new DataFileReader<>(TestContext.INSTANCE.testFile2(), reader)) {

            GenericRecord record = null;
            while (dataFileReader.hasNext()) {
                record = dataFileReader.next(record);
                assertThat(record.get("left").toString(), is("L"));
                assertThat(record.get("right").toString(), is("R"));
                //assertThat(record.get("description").toString(), is(""));
                assertThat(dataFileReader.hasNext(), is(false));
            }
        }

    }

    @Test
    public void nameAliases() throws Exception {

        //Create the parser for the schema
        Schema.Parser parser = new Schema.Parser();

        //Create schema from .avsc file
        Schema oldSchema = parser.parse(getClass().getResourceAsStream(TestContext.INSTANCE.stringPairSchemaFile()));

        //Create the parser for the schema
        Schema.Parser parser1 = new Schema.Parser();

        //Create schema from .avsc file
        Schema newSchema = parser1.parse(getClass().getResourceAsStream(TestContext.INSTANCE.nameAliases()));

        //Create the reader for a record
        DatumReader<GenericRecord> reader = new GenericDatumReader<>(oldSchema, newSchema);

        try (DataFileReader<GenericRecord> dataFileReader = new DataFileReader<>(TestContext.INSTANCE.testFile2(), reader)) {

            GenericRecord record = null;
            while (dataFileReader.hasNext()) {
                record = dataFileReader.next(record);
                System.out.println(record);

                assertThat(record.get("first").toString(), is("L"));
                assertThat(record.get("second").toString(), is("R"));
                //assertThat(record.get("description").toString(), is(""));
                //assertThat(dataFileReader.hasNext(), is(false));
            }
        }

    }
}
