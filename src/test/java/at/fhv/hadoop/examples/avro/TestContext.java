package at.fhv.hadoop.examples.avro;

import java.io.File;

public enum TestContext {
    INSTANCE;

    private String stringPairSchemaFile = "/avro/StringPair.avsc";
    private String extendedStringPairSchemaFile = "/avro/StringPair1.avsc";
    private String extendedStringPairSchemaFile1 = "/avro/StringPair2.avsc";
    private String nameAliases = "/avro/StringPairNameAliases.avsc";
    private String fileName = "testOutput/test";
    private String fileName1 = "testOutput/test1";
    private String fileName2 = "testOutput/data.avro";
    private String fileName3 = "testOutput/test2.json";



    public String stringPairSchemaFile() {
        return stringPairSchemaFile;
    }

    public String extendedStringPairSchemaFile() {
        return extendedStringPairSchemaFile;
    }

    public String extendedStringPairSchemaFile1() {
        return extendedStringPairSchemaFile1;
    }

    public String nameAliases() {
        return nameAliases;
    }


    public File testFile() {
        File testFile = new File(fileName);
        if (!testFile.getParentFile().exists()) {
            testFile.getParentFile().mkdirs();
        }
        return testFile;
    }

    public File testFile1() {
        File testFile = new File(fileName1);
        if (!testFile.getParentFile().exists()) {
            testFile.getParentFile().mkdirs();
        }
        return testFile;
    }


    public File testFile3() {
        File testFile = new File(fileName3);
        if (!testFile.getParentFile().exists()) {
            testFile.getParentFile().mkdirs();
        }
        return testFile;
    }

    public File testFile2() {
        File testFile = new File(fileName2);
        if (!testFile.getParentFile().exists()) {
            testFile.getParentFile().mkdirs();
        }
        return testFile;
    }
}
