package at.fhv.hadoop.examples.mean;

import at.fhv.hadoop.examples.mean.v0.ArithmeticMeanReducerV0;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mrunit.mapreduce.MultipleInputsMapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MeanTest {

    private static void setConfigurationValues(Configuration conf) {
        conf.setInt("NUMBERS_PER_MAP", 100);
        conf.setInt("MIN", 0);
        conf.setInt("MAX", 10);
    }


    private Mapper mapper;
    private MapDriver<Text, Text, Text, IntWritable> mapDriver;

    private Reducer reducer;
    private ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;

    private MapReduceDriver<Text, Text, Text, IntWritable, Text, IntWritable> mapReduceDriver;
    private MapReduceDriver<Text, Text, Text, IntWritable, Text, IntWritable> mapReduceDriverWrong;




    @Before
    public void setUp() throws Exception {

        mapper = new RandomIntegerMapper();
        mapDriver = MapDriver.newMapDriver(mapper);

        reducer = new ArithmeticMeanReducerV0();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);

        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);

        mapReduceDriverWrong = MapReduceDriver.newMapReduceDriver(mapper, reducer, reducer);
    }

    @Test
    public void testMapper() throws Exception {

        Configuration conf = mapDriver.getConfiguration();
        setConfigurationValues(conf);

        mapDriver.addInput(new Text("Test"), new Text("Test"));

        List<Pair<Text, IntWritable>> result = mapDriver.run();
        result.stream().map(res -> res.getFirst() + "," + res.getSecond()).forEach(System.out::println);
    }

    @Test
    public void testReducer() throws Exception {

        List<IntWritable> testList = Stream.of(1, 2, 3, 4, 5).map(IntWritable::new).collect(Collectors.toList());

        reduceDriver.addInput(new Text("A"), testList);

        List<Pair<Text, IntWritable>> result = reduceDriver.run();
        result.stream().map(res -> res.getFirst() + "," + res.getSecond()).forEach(System.out::println);
    }

    @Test
    public void testMapReduce() throws Exception {

        // Create the identity mapper
        Mapper<Text, IntWritable, Text, IntWritable> mapper = new Mapper<>();

        List<Pair<Text, IntWritable>> testList = Stream
                .of(1, 2, 3, 4, 5)
                .map(i -> new Pair<>(new Text("A"), new IntWritable(i)))
                .collect(Collectors.toList());


        MapReduceDriver<Text, IntWritable, Text, IntWritable, Text, IntWritable> mapReduceDriver1 = MapReduceDriver.newMapReduceDriver(mapper, reducer);

        mapReduceDriver1.addAll(testList);


        List<Pair<Text, IntWritable>> result = mapReduceDriver1.run();
        result.stream().map(res -> res.getFirst() + "," + res.getSecond()).forEach(System.out::println);
    }


    @Test
    public void testMapReduce1() throws Exception {

        // Create the identity mapper
        Mapper<Text, IntWritable, Text, IntWritable> mapper = new Mapper<>();

        List<Pair<Text, IntWritable>> testList = Stream
                .of(1, 2, 3, 4, 5)
                .map(i -> new Pair<>(new Text("A"), new IntWritable(i)))
                .collect(Collectors.toList());



        MapReduceDriver<Text, IntWritable, Text, IntWritable, Text, IntWritable> mapReduceDriver1 =
                MapReduceDriver.newMapReduceDriver(mapper, reducer, reducer);

        mapReduceDriver1.addAll(testList);


        List<Pair<Text, IntWritable>> result = mapReduceDriver1.run();
        result.stream().map(res -> res.getFirst() + "," + res.getSecond()).forEach(System.out::println);
    }


    @Test
    public void testMapReduce2() throws Exception {


        MultipleInputsMapReduceDriver mapReduceDriver1 = MultipleInputsMapReduceDriver.newMultipleInputMapReduceDriver(reducer);

        // Create the identity mapper
        Mapper<Text, IntWritable, Text, IntWritable> mapper0 = new Mapper<>();
        Mapper<Text, IntWritable, Text, IntWritable> mapper1 = new Mapper<>();


        List<Pair<Text, IntWritable>> testList0 = Stream
                .of(1, 2)
                .map(i -> new Pair<>(new Text("A"), new IntWritable(i)))
                .collect(Collectors.toList());

        List<Pair<Text, IntWritable>> testList1 = Stream
                .of(3,4,5)
                .map(i -> new Pair<>(new Text("A"), new IntWritable(i)))
                .collect(Collectors.toList());

        List<Pair<Text, IntWritable>> testList2 = Stream
                .of(1,4)
                .map(i -> new Pair<>(new Text("A"), new IntWritable(i)))
                .collect(Collectors.toList());



        mapReduceDriver1.addMapper(mapper0);
        mapReduceDriver1.addAll(mapper0, testList2);
        //mapReduceDriver1.addMapper(mapper1);
        //mapReduceDriver1.addAll(mapper1, testList1);



        List<Pair<Text, IntWritable>> result = mapReduceDriver1.run();
        result.stream().map(res -> res.getFirst() + "," + res.getSecond()).forEach(System.out::println);

    }














}
