package at.fhv.hadoop.examples.max_temperature_avro;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.hadoop.io.AvroSerialization;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.avro.mapreduce.AvroJob;
import org.apache.avro.mapreduce.AvroKeyOutputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.CommonConfigurationKeysPublic;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class MaxTemperatureAvroTest {


    private Mapper mapper;
    private MapDriver<LongWritable, Text, AvroKey<Integer>, AvroValue<GenericRecord>> mapDriver;

    private Reducer reducer;
    private MapReduceDriver<LongWritable, Text, Text, AvroKey<Integer>, AvroKey<GenericRecord>, NullWritable> mapReduceDriver;

    private MapDriver<LongWritable, Text, AvroKey<Integer>, AvroValue<GenericRecord>> configureMapDriver(Mapper mapper) throws Exception{
        MapDriver<LongWritable, Text, AvroKey<Integer>, AvroValue<GenericRecord>> mapDriver = MapDriver.newMapDriver(mapper);

        Configuration configuration = mapDriver.getConfiguration();

        // Add AvroSerialization to the configuration
        // (copy over the default serializations for deserializing the mapper inputs)
        String[] serializations = configuration.getStrings(CommonConfigurationKeysPublic.IO_SERIALIZATIONS_KEY);
        String[] newSerializations = Arrays.copyOf(serializations, serializations.length + 1);
        newSerializations[serializations.length] = AvroSerialization.class.getName();
        configuration.setStrings(CommonConfigurationKeysPublic.IO_SERIALIZATIONS_KEY, newSerializations);

        //Configure AvroSerialization by specifying the key writer and value writer schemas
        AvroSerialization.setKeyWriterSchema(configuration, Schema.create(Schema.Type.INT));
        AvroSerialization.setValueWriterSchema(configuration, Schemas.INSTANCE.weatherRecordSchema());

        return mapDriver;
    }

    private MapReduceDriver<LongWritable,Text,Text,AvroKey<Integer>,AvroKey<GenericRecord>,NullWritable> configureMapReduceDriver(Mapper mapper, Reducer reducer) {

        MapReduceDriver<LongWritable,Text,Text,AvroKey<Integer>,AvroKey<GenericRecord>,NullWritable> mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);

        Configuration configuration = mapReduceDriver.getConfiguration();

        // Add AvroSerialization to the configuration
        // (copy over the default serializations for deserializing the mapper inputs)
        String[] serializations = configuration.getStrings(CommonConfigurationKeysPublic.IO_SERIALIZATIONS_KEY);
        String[] newSerializations = Arrays.copyOf(serializations, serializations.length + 1);
        newSerializations[serializations.length] = AvroSerialization.class.getName();
        configuration.setStrings(CommonConfigurationKeysPublic.IO_SERIALIZATIONS_KEY, newSerializations);

        //Configure AvroSerialization by specifying the key writer and value writer schemas
        AvroSerialization.setKeyWriterSchema(configuration, Schema.create(Schema.Type.INT));
        AvroSerialization.setValueWriterSchema(configuration, Schemas.INSTANCE.weatherRecordSchema());

        return mapReduceDriver;
    }



    @Before
    public void setUp() throws Exception {
        mapper = new NCDCRecordToAvroMapper();

        mapDriver = configureMapDriver(mapper);

        reducer = new MaxTemperatureAvroReducer();

        mapReduceDriver = configureMapReduceDriver(mapper, reducer);
    }

    @Test
    public void processValidRecord() throws Exception {
        Text value = new Text("0029029070999991901010106004+64333+023450FM-12+" +
                // Year^^^^
                "000599999V0202701N015919999999N0000001N9-00781+99999102001ADDGF108991999999999999999999");
        //Temperature^^^^^

        //Add input
        mapDriver.withInput(new LongWritable(0), value);


        //Create expected output
        List<Pair<AvroKey<Integer>, AvroValue<GenericRecord>>> expectedOutput = new ArrayList<>();

        {
            GenericRecord expectedValue = new GenericData.Record(Schemas.INSTANCE.weatherRecordSchema());
            expectedValue.put("year", 1901);
            expectedValue.put("temperature", -78);
            expectedValue.put("stationId", "029070-99999");

            expectedOutput.add(new Pair<>(new AvroKey<>(1901), new AvroValue<>(expectedValue)));
        }

        List<Pair<AvroKey<Integer>, AvroValue<GenericRecord>>> result = mapDriver.run();

        assertEquals("Different length", result.size(), expectedOutput.size());

        for (int i = 0; i < expectedOutput.size(); i++) {
            AvroKey<Integer> actualKey = result.get(i).getFirst();
            AvroKey<Integer> expectedKey = expectedOutput.get(i).getFirst();

            assertThat(actualKey.datum(), is(expectedKey.datum()));

            AvroValue<GenericRecord> actualValue = result.get(i).getSecond();
            AvroValue<GenericRecord> expectedValue = expectedOutput.get(i).getSecond();

            assertThat(actualValue.datum().get("year"), is(expectedValue.datum().get("year")));
            assertThat(actualValue.datum().get("temperature"), is(expectedValue.datum().get("temperature")));
            assertThat(actualValue.datum().get("stationId"), is(expectedValue.datum().get("stationId")));
        }
    }

    @Test
    public void testMap() throws Exception {
        mapDriver.withAll(TestContext.INSTANCE.mapInputLines());
        List<Pair<AvroKey<Integer>, AvroValue<GenericRecord>>> result = mapDriver.run();
    }

    @Test
    public void testReduce() throws Exception {
        //reduceDriver.
    }



    @Test
    public void testMapReduce() throws Exception {

        //Add expected input
        mapReduceDriver.withAll(TestContext.INSTANCE.mapInputLines());


        mapReduceDriver.run();

    }
}
