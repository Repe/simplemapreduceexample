package at.fhv.hadoop.examples.word_co_occurrence;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class WordOccurrenceTest {

    private Mapper pairsMapper;
    private MapDriver<LongWritable, Text, TextPair, IntWritable> pairsMapDriver;

    private Reducer pairsReducer;
    private MapReduceDriver<LongWritable, Text, TextPair, IntWritable, TextPair, IntWritable> pairsMapReduceDriver;
    

    private Mapper stripesMapper;
    private MapDriver<LongWritable, Text, Text, MapWritable> stripesMapDriver;

    private Reducer stripesReducer;
    private MapReduceDriver<LongWritable, Text, Text, MapWritable, Text, MapWritable> stripesMapReduceDriver;


    @Before
    public void setUp() throws Exception {
        pairsMapper = new PairsMapper();
        pairsMapDriver = MapDriver.newMapDriver(pairsMapper);
        
        pairsReducer = new PairsReducer();
        pairsMapReduceDriver = MapReduceDriver.newMapReduceDriver(pairsMapper, pairsReducer);


        stripesMapper = new StripesMapper();
        stripesMapDriver = MapDriver.newMapDriver(stripesMapper);

        stripesReducer = new StripesReducer();
        stripesMapReduceDriver = MapReduceDriver.newMapReduceDriver(stripesMapper, stripesReducer);

    }

    @Test
    public void testPairsMapper() throws Exception {
        pairsMapDriver.withAll(TestContext.INSTANCE.mapInput());

        List<Pair<TextPair, IntWritable>> result = pairsMapDriver.run();

        result.stream().map(entry -> entry.getFirst() + "," + entry.getSecond()).forEach(System.out::println);
    }

    @Test
    public void testPairsMapReduce() throws Exception {
    
        pairsMapReduceDriver.addAll(TestContext.INSTANCE.mapInput());

        List<Pair<TextPair, IntWritable>> result = pairsMapReduceDriver.run();

        result.stream().map(entry -> entry.getFirst() + "," + entry.getSecond()).forEach(System.out::println);
    }

    @Test
    public void testStripesMapper() throws Exception {

        stripesMapDriver.addAll(TestContext.INSTANCE.mapInput());

        List<Pair<Text, MapWritable>> result = stripesMapDriver.run();

        result.stream().map(entry -> entry.getFirst() + ", " + entry.getSecond()).forEach(System.out::println);
    }

    @Test
    public void testStripesMapReduce() throws Exception {

        stripesMapReduceDriver.addAll(TestContext.INSTANCE.mapInput());

        List<Pair<Text, MapWritable>> result = stripesMapReduceDriver.run();

        result.stream().map(entry -> entry.getFirst() + ", " + entry.getSecond()).forEach(System.out::println);
    }
}
