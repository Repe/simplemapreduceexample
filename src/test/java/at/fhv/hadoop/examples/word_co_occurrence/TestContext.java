package at.fhv.hadoop.examples.word_co_occurrence;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.types.Pair;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public enum TestContext {
    INSTANCE;

    private String mapInputFileName() {
        return "/word_co_occurence/input.txt";
    }

    public List<Pair<LongWritable, Text>> mapInput() throws URISyntaxException, IOException {

        URL test = TestContext.class.getResource(mapInputFileName());

        Text testText = new Text(Files.readAllBytes(Paths.get(test.toURI())));

        return Arrays.asList(new Pair<>(new LongWritable(1), testText));
    }


}
