package at.fhv.hadoop.examples.stats;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class StatisticsTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void binomialCoefficient() throws Exception {
        {
            long n = 0;
            long k = 0;
            long actual = Statistics.binomialCoefficient(n,k);
            long expected = 1;
            assertEquals(expected, actual);
        }
        {
            long n = 1;
            long k = 0;
            long actual = Statistics.binomialCoefficient(n,k);
            long expected = 1;
            assertEquals(expected, actual);
        }
        {
            long n = 1;
            long k = 1;
            long actual = Statistics.binomialCoefficient(n,k);
            long expected = 1;
            assertEquals(expected, actual);
        }
        {
            long n = 2;
            long k = 0;
            long actual = Statistics.binomialCoefficient(n,k);
            long expected = 1;
            assertEquals(expected, actual);
        }
        {
            long n = 2;
            long k = 1;
            long actual = Statistics.binomialCoefficient(n,k);
            long expected = 2;
            assertEquals(expected, actual);
        }
        {
            long n = 2;
            long k = 2;
            long actual = Statistics.binomialCoefficient(n,k);
            long expected = 1;
            assertEquals(expected, actual);
        }
        {
            long n = 3;
            long k = 0;
            long actual = Statistics.binomialCoefficient(n,k);
            long expected = 1;
            assertEquals(expected, actual);
        }
        {
            long n = 3;
            long k = 1;
            long actual = Statistics.binomialCoefficient(n,k);
            long expected = 3;
            assertEquals(expected, actual);
        }
        {
            long n = 3;
            long k = 2;
            long actual = Statistics.binomialCoefficient(n,k);
            long expected = 3;
            assertEquals(expected, actual);
        }
        {
            long n = 3;
            long k = 3;
            long actual = Statistics.binomialCoefficient(n,k);
            long expected = 1;
            assertEquals(expected, actual);
        }
        {
            long n = 4;
            long k = 0;
            long actual = Statistics.binomialCoefficient(n,k);
            long expected = 1;
            assertEquals(expected, actual);
        }
        {
            long n = 4;
            long k = 1;
            long actual = Statistics.binomialCoefficient(n,k);
            long expected = 4;
            assertEquals(expected, actual);
        }
        {
            long n = 4;
            long k = 2;
            long actual = Statistics.binomialCoefficient(n,k);
            long expected = 6;
            assertEquals(expected, actual);
        }
        {
            long n = 4;
            long k = 3;
            long actual = Statistics.binomialCoefficient(n,k);
            long expected = 4;
            assertEquals(expected, actual);
        }
        {
            long n = 4;
            long k = 4;
            long actual = Statistics.binomialCoefficient(n,k);
            long expected = 1;
            assertEquals(expected, actual);
        }
    }

    @Test
    public void average() throws Exception {
        double delta = 0.0;

        {
            double[] values = new double[]{};
            double actual = Statistics.average(values);
            double expected = Double.NaN;
            assertEquals(expected, actual, delta);
        }

        {
            double[] values = new double[]{1.0};
            double actual = Statistics.average(values);
            double expected = values[0];
            assertEquals(expected, actual, delta);
        }

        {
            double[] values = new double[]{1.0, 2.0};
            double actual = Statistics.average(values);
            double expected = 1.5;
            assertEquals(expected, actual, delta);
        }

        {
            double[] values = new double[]{-1.0, -2.0};
            double actual = Statistics.average(values);
            double expected = -1.5;
            assertEquals(expected, actual, delta);
        }

        {
            double[] values = new double[]{-1.0, 2.0};
            double actual = Statistics.average(values);
            double expected = 0.5;
            assertEquals(expected, actual, delta);
        }









    }



}