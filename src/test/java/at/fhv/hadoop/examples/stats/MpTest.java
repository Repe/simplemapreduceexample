package at.fhv.hadoop.examples.stats;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MpTest {
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    private void assertMp(DescriptiveStatistics.Mp mp, int expectedOrder, double expectedValue, int expectedSize, double expectedAverage) {
        double delta = 0.0;
        Assert.assertEquals("Order: ", expectedOrder, mp.order());
        Assert.assertEquals("Value: ", expectedValue, mp.value(), delta);
        Assert.assertEquals("Size:", expectedSize, mp.size());
        Assert.assertEquals("Average", expectedAverage, mp.average(), delta);
    }

    private void testOrder(double[] values, int order, double expectedValue, double expectedAverage) {
        double average = Statistics.average(values);
        DescriptiveStatistics.Mp actual = DescriptiveStatistics.Mp.calculate(order, values, average);

        int expectedSize = values.length;
        int expectedOrder = order;
        assertMp(actual, expectedOrder, expectedValue, expectedSize, expectedAverage);
        System.out.println(actual);
    }

    @Test
    public void calculateOrder0() throws Exception {
        int order = 0;

        {
            double[] values = new double[]{};
            double expectedValue = Double.NaN;
            double expectedAverage = Double.NaN;
            testOrder(values, order, expectedValue, expectedAverage);
        }

        {
            double[] values = new double[]{1.0};
            double expectedValue = 1.0;
            double expectedAverage = 1.0;
            testOrder(values, order, expectedValue, expectedAverage);
        }

        {
            double[] values = new double[]{1.0, 2.0};
            double expectedValue = 2.0;
            double expectedAverage = 1.5;
            testOrder(values, order, expectedValue, expectedAverage);
        }

        {
            double[] values = new double[]{-1.0, -2.0};
            double expectedValue = 2.0;
            double expectedAverage = -1.5;
            testOrder(values, order, expectedValue, expectedAverage);
        }

        {
            double[] values = new double[]{-1.0, 2.0};
            double expectedValue = 2.0;
            double expectedAverage = 0.5;
            testOrder(values, order, expectedValue, expectedAverage);
        }
    }

    @Test
    public void calculateOrder1() throws Exception {
        int order = 1;
        {
            double[] values = new double[]{};
            double expectedValue = Double.NaN;
            double expectedAverage = Double.NaN;
            testOrder(values, order, expectedValue, expectedAverage);
        }

        {
            double[] values = new double[]{1.0};
            double expectedValue = 0.0;
            double expectedAverage = 1.0;
            testOrder(values, order, expectedValue, expectedAverage);
        }


        {
            double[] values = new double[]{1.0, 2.0};
            double expectedValue = 0.0;
            double expectedAverage = 1.5;
            testOrder(values, order, expectedValue, expectedAverage);
        }


        {
            double[] values = new double[]{-1.0, -2.0};
            double expectedValue = 0.0;
            double expectedAverage = -1.5;
            testOrder(values, order, expectedValue, expectedAverage);
        }


        {
            double[] values = new double[]{-1.0, 2.0};
            double expectedValue = 0.0;
            double expectedAverage = 0.5;
            testOrder(values, order, expectedValue, expectedAverage);
        }
    }

    @Test
    public void calculateOrder2() throws Exception {
        int order = 2;

        {
            double[] values = new double[]{};
            double expectedValue = Double.NaN;
            double expectedAverage = Double.NaN;
            testOrder(values, order, expectedValue, expectedAverage);
        }


        {
            double[] values = new double[]{1.0};
            double expectedValue = 0.0;
            double expectedAverage = 1.0;
            testOrder(values, order, expectedValue, expectedAverage);
        }


        {
            double[] values = new double[]{1.0, 2.0};
            double expectedValue = 0.5;
            double expectedAverage = 1.5;
            testOrder(values, order, expectedValue, expectedAverage);
        }


        {
            double[] values = new double[]{-1.0, -2.0};
            double expectedValue = 0.5;
            double expectedAverage = -1.5;
            testOrder(values, order, expectedValue, expectedAverage);
        }


        {
            double[] values = new double[]{-1.0, 2.0};
            double expectedValue = 4.5;
            double expectedAverage = 0.5;
            testOrder(values, order, expectedValue, expectedAverage);
        }
    }

    @Test
    public void calculateOrder3() throws Exception {
        int order = 3;

        {
            double[] values = new double[]{};
            double expectedValue = Double.NaN;
            double expectedAverage = Double.NaN;
            testOrder(values, order, expectedValue, expectedAverage);
        }


        {
            double[] values = new double[]{1.0};
            double expectedValue = 0.0;
            double expectedAverage = 1.0;
            testOrder(values, order, expectedValue, expectedAverage);
        }


        {
            double[] values = new double[]{1.0, 2.0};
            double expectedValue = 0.0;
            double expectedAverage = 1.5;
            testOrder(values, order, expectedValue, expectedAverage);
        }


        {
            double[] values = new double[]{-1.0, -2.0};
            double expectedValue = 0.0;
            double expectedAverage = -1.5;
            testOrder(values, order, expectedValue, expectedAverage);
        }


        {
            double[] values = new double[]{-1.0, 2.0};
            double expectedValue = 0.0;
            double expectedAverage = 0.5;
            testOrder(values, order, expectedValue, expectedAverage);
        }
    }

    @Test
    public void calculateOrder4() throws Exception {
        int order = 4;

        {
            double[] values = new double[]{};
            double expectedValue = Double.NaN;
            double expectedAverage = Double.NaN;
            testOrder(values, order, expectedValue, expectedAverage);
        }


        {
            double[] values = new double[]{1.0};
            double expectedValue = 0.0;
            double expectedAverage = 1.0;
            testOrder(values, order, expectedValue, expectedAverage);
        }


        {
            double[] values = new double[]{1.0, 2.0};
            double expectedValue = 0.125;
            double expectedAverage = 1.5;
            testOrder(values, order, expectedValue, expectedAverage);
        }


        {
            double[] values = new double[]{-1.0, -2.0};
            double expectedValue = 0.125;
            double expectedAverage = -1.5;
            testOrder(values, order, expectedValue, expectedAverage);
        }


        {
            double[] values = new double[]{-1.0, 2.0};
            double expectedValue = 10.125;
            double expectedAverage = 0.5;
            testOrder(values, order, expectedValue, expectedAverage);
        }
    }

}