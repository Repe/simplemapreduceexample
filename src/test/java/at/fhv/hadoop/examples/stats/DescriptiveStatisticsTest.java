package at.fhv.hadoop.examples.stats;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class DescriptiveStatisticsTest {


    private static DescriptiveStatistics.Mp createTestInstanceEmptySet(int order) {
        return createTestInstance(order, new double[0]);
    }

    private static DescriptiveStatistics.Mp createTestInstance(int order, double[] values) {
        double average = Statistics.average(values);
        DescriptiveStatistics.Mp instance = DescriptiveStatistics.Mp.calculate(order, values, average);
        return instance;
    }

    private void assertMp(DescriptiveStatistics.Mp mp, int expectedOrder, double expectedValue, int expectedSize, double expectedAverage) {
        double delta = 0.00000001;
        Assert.assertEquals("Order: ", expectedOrder, mp.order());
        Assert.assertEquals("Value: ", expectedValue, mp.value(), delta);
        Assert.assertEquals("Size:", expectedSize, mp.size());
        Assert.assertEquals("Average", expectedAverage, mp.average(), delta);
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    private void testUpdateFormula(DescriptiveStatistics.Mp a, DescriptiveStatistics.Mp b, double expectedValue, double expectedAverage) {
        int expectedOrder = a.order();
        int expectedSize = a.size() + b.size();
        DescriptiveStatistics.Mp actual = DescriptiveStatistics.updateFormula(a,b);
        assertMp(actual, expectedOrder, expectedValue, expectedSize, expectedAverage);
        System.out.println(actual);
    }

    private double[] concat(double[] a, double[] b) {
        int aLen = a.length;
        int bLen = b.length;

        double[] c = new double[aLen + bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }



    @Test
    public void updateFormula0() throws Exception {

        int order = 0;

        double[][] testSets = new double[][]{
                new double[]{},
                new double[]{1.0},
                new double[]{1.0, 2.0},
                new double[]{-1.0, -2.0},
                new double[]{-1.0, 2.0}
        };

        double[][] resultSets = new double[testSets.length * testSets.length][];

        int resCounter = 0;
        for (int i = 0, testSetsLength = testSets.length; i < testSetsLength; i++) {
            double[] testSeti = testSets[i];
            for (int i1 = 0, testSetsLength1 = testSets.length; i1 < testSetsLength1; i1++) {
                double[] testSetj = testSets[i1];
                double[] conc = concat(testSeti, testSetj);
                resultSets[resCounter] = conc;
                resCounter++;
            }
        }

        resCounter = 0;
        for (int i = 0, testSetsLength = testSets.length; i < testSetsLength; i++) {
            double[] setA = testSets[i];
            for (int i1 = 0, testSetsLength1 = testSets.length; i1 < testSetsLength1; i1++) {
                double[] setB = testSets[i1];
                DescriptiveStatistics.Mp a = createTestInstance(order, setA);
                DescriptiveStatistics.Mp b = createTestInstance(order, setB);
                double[] setC = resultSets[resCounter];
                DescriptiveStatistics.Mp expected = createTestInstance(order, setC);
                double expectedValue = expected.value();
                double expectedAverage = expected.average();
                testUpdateFormula(a, b, expectedValue, expectedAverage);
                resCounter++;
            }
        }
    }

    @Test
    public void updateFormula1() throws Exception {

        int order = 1;

        double[][] testSets = new double[][]{
                new double[]{},
                new double[]{1.0},
                new double[]{1.0, 2.0},
                new double[]{-1.0, -2.0},
                new double[]{-1.0, 2.0}
        };

        double[][] resultSets = new double[testSets.length * testSets.length][];

        int resCounter = 0;
        for (int i = 0, testSetsLength = testSets.length; i < testSetsLength; i++) {
            double[] testSeti = testSets[i];
            for (int i1 = 0, testSetsLength1 = testSets.length; i1 < testSetsLength1; i1++) {
                double[] testSetj = testSets[i1];
                double[] conc = concat(testSeti, testSetj);
                resultSets[resCounter] = conc;
                resCounter++;
            }
        }

        resCounter = 0;
        for (int i = 0, testSetsLength = testSets.length; i < testSetsLength; i++) {
            double[] setA = testSets[i];
            for (int i1 = 0, testSetsLength1 = testSets.length; i1 < testSetsLength1; i1++) {
                double[] setB = testSets[i1];
                DescriptiveStatistics.Mp a = createTestInstance(order, setA);
                DescriptiveStatistics.Mp b = createTestInstance(order, setB);
                System.out.println("a: " + a);
                System.out.println("b: " + b);

                double[] setC = resultSets[resCounter];
                DescriptiveStatistics.Mp expected = createTestInstance(order, setC);
                double expectedValue = expected.value();
                double expectedAverage = expected.average();
                testUpdateFormula(a, b, expectedValue, expectedAverage);
                resCounter++;
            }
        }
    }


    @Test
    public void updateFormula2() throws Exception {

        int order = 2;

        double[][] testSets = new double[][]{
                new double[]{},
                new double[]{1.0},
                new double[]{1.0, 2.0},
                new double[]{-1.0, -2.0},
                new double[]{-1.0, 2.0}
        };

        double[][] resultSets = new double[testSets.length * testSets.length][];

        int resCounter = 0;
        for (int i = 0, testSetsLength = testSets.length; i < testSetsLength; i++) {
            double[] testSeti = testSets[i];
            for (int i1 = 0, testSetsLength1 = testSets.length; i1 < testSetsLength1; i1++) {
                double[] testSetj = testSets[i1];
                double[] conc = concat(testSeti, testSetj);
                resultSets[resCounter] = conc;
                resCounter++;
            }
        }

        resCounter = 0;
        for (int i = 0, testSetsLength = testSets.length; i < testSetsLength; i++) {
            double[] setA = testSets[i];
            for (int i1 = 0, testSetsLength1 = testSets.length; i1 < testSetsLength1; i1++) {
                double[] setB = testSets[i1];
                DescriptiveStatistics.Mp a = createTestInstance(order, setA);
                DescriptiveStatistics.Mp b = createTestInstance(order, setB);
                System.out.println("a: " + a);
                System.out.println("b: " + b);

                double[] setC = resultSets[resCounter];
                DescriptiveStatistics.Mp expected = createTestInstance(order, setC);
                double expectedValue = expected.value();
                double expectedAverage = expected.average();
                testUpdateFormula(a, b, expectedValue, expectedAverage);
                resCounter++;
            }
        }
    }


    @Test
    public void updateFormula3() throws Exception {

        int order = 3;

        double[][] testSets = new double[][]{
                new double[]{},
                new double[]{1.0},
                new double[]{1.0, 2.0},
                new double[]{-1.0, -2.0},
                new double[]{-1.0, 2.0}
        };

        double[][] resultSets = new double[testSets.length * testSets.length][];

        int resCounter = 0;
        for (int i = 0, testSetsLength = testSets.length; i < testSetsLength; i++) {
            double[] testSeti = testSets[i];
            for (int i1 = 0, testSetsLength1 = testSets.length; i1 < testSetsLength1; i1++) {
                double[] testSetj = testSets[i1];
                double[] conc = concat(testSeti, testSetj);
                resultSets[resCounter] = conc;
                resCounter++;
            }
        }

        resCounter = 0;
        for (int i = 0, testSetsLength = testSets.length; i < testSetsLength; i++) {
            double[] setA = testSets[i];
            for (int i1 = 0, testSetsLength1 = testSets.length; i1 < testSetsLength1; i1++) {
                double[] setB = testSets[i1];
                DescriptiveStatistics.Mp a = createTestInstance(order, setA);
                DescriptiveStatistics.Mp b = createTestInstance(order, setB);
                //System.out.println("a: " + a);
                //System.out.println("b: " + b);

                double[] setC = resultSets[resCounter];
                //System.out.println("Set A:" + Arrays.toString(setA));
                //System.out.println("Set B:" + Arrays.toString(setB));
                //System.out.println("Set C:" + Arrays.toString(setC));



                DescriptiveStatistics.Mp expected = createTestInstance(order, setC);
                double expectedValue = expected.value();
                double expectedAverage = expected.average();
                testUpdateFormula(a, b, expectedValue, expectedAverage);
                resCounter++;
            }
        }
    }


    @Test
    public void updateFormula4() throws Exception {

        int order = 4;

        double[][] testSets = new double[][]{
                new double[]{},
                new double[]{1.0},
                new double[]{1.0, 2.0},
                new double[]{-1.0, -2.0},
                new double[]{-1.0, 2.0}
        };

        double[][] resultSets = new double[testSets.length * testSets.length][];

        int resCounter = 0;
        for (int i = 0, testSetsLength = testSets.length; i < testSetsLength; i++) {
            double[] testSeti = testSets[i];
            for (int i1 = 0, testSetsLength1 = testSets.length; i1 < testSetsLength1; i1++) {
                double[] testSetj = testSets[i1];
                double[] conc = concat(testSeti, testSetj);
                resultSets[resCounter] = conc;
                resCounter++;
            }
        }

        resCounter = 0;
        for (int i = 0, testSetsLength = testSets.length; i < testSetsLength; i++) {
            double[] setA = testSets[i];
            for (int i1 = 0, testSetsLength1 = testSets.length; i1 < testSetsLength1; i1++) {
                double[] setB = testSets[i1];
                DescriptiveStatistics.Mp a = createTestInstance(order, setA);
                DescriptiveStatistics.Mp b = createTestInstance(order, setB);
                System.out.println("a: " + a);
                System.out.println("b: " + b);

                double[] setC = resultSets[resCounter];
                System.out.println("Set A:" + Arrays.toString(setA));
                System.out.println("Set B:" + Arrays.toString(setB));
                System.out.println("Set C:" + Arrays.toString(setC));



                DescriptiveStatistics.Mp expected = createTestInstance(order, setC);
                double expectedValue = expected.value();
                double expectedAverage = expected.average();
                testUpdateFormula(a, b, expectedValue, expectedAverage);
                resCounter++;
            }
        }
    }

    private void assertMp1(DescriptiveStatistics.Mp expected, DescriptiveStatistics.Mp actual) {
        double delta = 0.00000001;
        Assert.assertNotNull("Actual null", actual);
        Assert.assertEquals("Order: ", expected.order(), actual.order());
        Assert.assertEquals("Value: ", expected.value(), actual.value(), delta);
        Assert.assertEquals("Size:", expected.size(), actual.size());
        Assert.assertEquals("Average", expected.average(), actual.average(), delta);
    }


    @Test
    public void calculateMsOfLowerOrder1() throws Exception {
        double[] testSetA = new double[]{1.0};
        double[] testSetB = new double[]{1.0, 2.0};
        double[] resultSet = new double[]{1.0, 1.0, 2.0};

        int order = 2;
        DescriptiveStatistics.Mp a = createTestInstance(order, testSetA);
        DescriptiveStatistics.Mp b = createTestInstance(order, testSetB);
        DescriptiveStatistics.Mp result = createTestInstance(order, resultSet);

        System.out.println("MpA: " + a);
        System.out.println("MpB: " + a);
        System.out.println("MpExpected: " + result);

        DescriptiveStatistics.Mp[] expected = result.msOfLowerOrder();
        DescriptiveStatistics.Mp[] actual = DescriptiveStatistics.calculateMsOfLowerOrder(a, b);

        Assert.assertNotNull("Result null", actual);
        Assert.assertTrue("Equal length", expected.length == actual.length);

        for (int i = 0; i < actual.length; i++) {
            DescriptiveStatistics.Mp expectedMp = expected[i];
            DescriptiveStatistics.Mp actualMp = actual[i];
            System.out.println("Expected: " + expectedMp);
            System.out.println("ActualMp: " + actualMp);
            assertMp1(expectedMp, actualMp);
        }




        System.out.println("Actual: " + Arrays.toString(actual));







    }






    @Test
    public void averageUpdateFormula() throws Exception {
        double averageA = 2;
        int sizeA = 3;
        double averageB = 4.5;
        int sizeB = 2;

        double actual = DescriptiveStatistics.averageUpdateFormula(averageA, sizeA, averageB, sizeB);
        double expected = 3.0;


        assertEquals("Test", expected, actual, 0.0);
    }

}