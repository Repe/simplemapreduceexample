package at.fhv.hadoop.examples.word_co_occurence_relative;

import at.fhv.hadoop.examples.word_co_occurrence_relative.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordOccurrenceTest {

    private Mapper pairsMapper;
    private MapDriver<LongWritable, Text, TextPair, IntWritable> pairsMapDriver;

    private Reducer pairsReducer;
    private MapReduceDriver<LongWritable, Text, TextPair, IntWritable, TextPair, DoubleWritable> pairsMapReduceDriver;
    

    private Mapper stripesMapper;
    private MapDriver<LongWritable, Text, Text, MapWritable> stripesMapDriver;

    private Reducer stripesReducer;
    private MapReduceDriver<LongWritable, Text, Text, MapWritable, Text, MapWritable> stripesMapReduceDriver;


    @Before
    public void setUp() throws Exception {

        pairsMapper = new PairsMapper();
        pairsMapDriver = MapDriver.newMapDriver(pairsMapper);
        
        pairsReducer = new PairsReducer();
        pairsMapReduceDriver = MapReduceDriver.newMapReduceDriver(pairsMapper, pairsReducer);


        stripesMapper = new StripesMapper();
        stripesMapDriver = MapDriver.newMapDriver(stripesMapper);

        stripesReducer = new StripesReducer();
        stripesMapReduceDriver = MapReduceDriver.newMapReduceDriver(stripesMapper, stripesReducer);

    }

    @Test
    public void testPairsMapper() throws Exception {
        pairsMapDriver.withAll(TestContext.INSTANCE.mapInput());




        List<Pair<TextPair, IntWritable>> result = pairsMapDriver.run();

        result.stream().map(entry -> entry.getFirst() + "," + entry.getSecond()).forEach(System.out::println);
    }

    @Test
    public void testPairsMapReduce() throws Exception {

        pairsMapReduceDriver.addAll(TestContext.INSTANCE.mapInput());

        pairsMapReduceDriver.withKeyOrderComparator(new TextPair.KeyComparator());

        List<Pair<TextPair, DoubleWritable>> result = pairsMapReduceDriver.run();

        result.stream().map(entry -> entry.getFirst() + "," + entry.getSecond()).forEach(System.out::println);
    }

    @Test
    public void testStripesMapper() throws Exception {

        stripesMapDriver.addAll(TestContext.INSTANCE.mapInput());

        List<Pair<Text, MapWritable>> result = stripesMapDriver.run();

        //result.stream().map(entry -> entry.getFirst() + ", " + entry.getSecond()).forEach(System.out::println);


        HashMap<String, Integer> counts = new HashMap<>();
        for (Pair<Text, MapWritable> entry : result) {

            String w1 = entry.getFirst().toString();

            Integer count = counts.getOrDefault(w1, 0);
            counts.put(w1, count + 1);
        }


        for (Map.Entry<String, Integer> co : counts.entrySet()) {
            System.out.println(co.getKey() + ": " + co.getValue());
        }

    }

    @Test
    public void testStripesMapReduce() throws Exception {

        stripesMapReduceDriver.addAll(TestContext.INSTANCE.mapInput());

        List<Pair<Text, MapWritable>> result = stripesMapReduceDriver.run();

        result.stream().map(entry -> entry.getFirst() + ", " + entry.getSecond()).forEach(System.out::println);
    }
}
