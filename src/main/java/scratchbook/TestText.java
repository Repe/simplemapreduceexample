package scratchbook;

import org.apache.hadoop.io.Text;

import java.util.HashMap;

public class TestText {

    private static void testHashMapWithTextAsKey() {
        HashMap<Text, Integer> testMap = new HashMap<>();

        Text testText = new Text("Test");

        testMap.put(testText, 1);
        System.out.println(testMap.size());
        System.out.println(testMap.get(new Text("Test")));
    }



    public static void main(String[] args) {
        System.out.println("Text Hadoop Text");
        testHashMapWithTextAsKey();
    }



}
