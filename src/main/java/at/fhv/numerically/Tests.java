package at.fhv.numerically;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

import static java.lang.Math.pow;

public class Tests {

    private final static Logger LOGGER = LoggerFactory.getLogger(Tests.class);

    private static String[] toHexString(double[] values) {
        String res[] = new String[values.length];
        for (int i = 0 ; i < values.length; i++) {
            res[i] = Long.toHexString(Double.doubleToRawLongBits(values[i]));
        }
        return res;
    }

    public static void main(String[] args) {
        LOGGER.trace("Entering main(args: {})", Arrays.toString(args));
        double[] x = new double[]{1.0, 1.0 + pow(10, -13), 1.0 + pow(10, -13), 1.0 + pow(10, -13)};
        String[] r = toHexString(x);
        double[] x_squared = new double[]{x[0]*x[0], x[1]*x[1], x[2]*x[2],x[3]*x[3]};
        String[] r_squared =toHexString(x_squared);

        for (int i = 0; i < x.length; i++) {
            System.out.println("x" + i + ": " + x[i] +  " r" + i + ": " + r[i] + " x_squared: " + x_squared[i] + " r_squared: " + r_squared[i]);
        }

        double sumx = x[0] + x[1] + x[2] + x[3];
        System.out.println("Sumx: " + sumx + " Hex: " + Long.toHexString(Double.doubleToRawLongBits(sumx)));
        double mean = sumx / 4.0;
        System.out.println("Mean: " + mean + " Hex: " + Long.toHexString(Double.doubleToRawLongBits(mean)));

        double sumx_squared = x_squared[0] + x_squared[1] + x_squared[2] + x_squared[3];
        System.out.println("sumx_squared: " + sumx_squared + " Hex: " + Long.toHexString(Double.doubleToRawLongBits(sumx_squared)));
        double mean_x_squared = sumx_squared / 4.0;
        System.out.println("Mean: " +  mean_x_squared + " Hex: " + Long.toHexString(Double.doubleToRawLongBits(mean_x_squared)));

        double var = mean_x_squared - mean * mean;
        System.out.println("Var: " + var + " Hex: " + Long.toHexString(Double.doubleToRawLongBits(var)));

        LOGGER.trace("Leaving main()");
    }



    public static void main1(String[] args) {
        double[] x = new double[]{1.0, 1.0 + pow(10, -13)};
        System.out.println(Long.toHexString(Double.doubleToRawLongBits(x[0])));
        System.out.println(Long.toHexString(Double.doubleToRawLongBits(x[1])));
        System.out.println(Long.toHexString(Double.doubleToRawLongBits(2.0 + pow(10, -13))));
    }



}
