package at.fhv.numerically;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

import java.util.Arrays;

public class TestsRedis {

    private final static Logger LOGGER = LoggerFactory.getLogger(TestsRedis.class);

    private static class RedisClientTest implements Runnable {

        private final Logger logger = LoggerFactory.getLogger(RedisClientTest.class);

        public static RedisClientTest create() {
            return new RedisClientTest();
        }

        private RedisClientTest() {}

        @Override
        public void run() {
            logger.trace("Entering run()");
            Jedis jedis = new Jedis("localhost", 63790);
            jedis.auth("redis");
            System.out.println("Connected");
            Transaction transaction = jedis.multi();
            transaction.set("inc","1");
            transaction.incrBy("inc", 1);
            transaction.incrBy("inc", 1);
            transaction.exec();
            System.out.println("Transaction ok");
            System.out.println("Result: " + jedis.get("inc"));
            logger.trace("Leaving run()");
        }
    }



    public static void main(String[] args) {
        LOGGER.trace("Entering main(args: {})", Arrays.toString(args));
        RedisClientTest redisClientTest = RedisClientTest.create();
        redisClientTest.run();
        LOGGER.trace("Leaving main()");
    }






}
