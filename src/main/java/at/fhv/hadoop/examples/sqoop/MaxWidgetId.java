package at.fhv.hadoop.examples.sqoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.sqoop.lib.RecordParser;

import java.io.IOException;

public class MaxWidgetId extends Configured implements Tool {

    public static class MaxWidgetMapper extends Mapper<LongWritable, Text, LongWritable, Widget> {

        private Widget maxWidget = null;

        public void map(LongWritable k, Text v, Context context) {
            Widget widget = new Widget();
            try {
                widget.parse(v); // Auto-generated: parse all fields from text.
            } catch (RecordParser.ParseError pe) {
                // Got a malformed record. Ignore it.
                return;
            }

            Integer id = widget.get_id();
            if (null == id) {
                return;
            } else {
                if (maxWidget == null
                        || id.intValue() > maxWidget.get_id().intValue()) {
                    maxWidget = widget;
                }
            }
        }

        public void cleanup(Context context)
                throws IOException, InterruptedException {
            if (null != maxWidget) {
                context.write(new LongWritable(0), maxWidget);
            }
        }


    }

    public static class MaxWidgetReducer extends Reducer<LongWritable, Widget, Widget, NullWritable> {

        public void reduce(LongWritable k, Iterable<Widget> vals, Context context)
                throws IOException, InterruptedException {
            Widget maxWidget = null;

            for (Widget w : vals) {
                if (maxWidget == null
                        || w.get_id().intValue() > maxWidget.get_id().intValue()) {
                    try {
                        maxWidget = (Widget) w.clone();
                    } catch (CloneNotSupportedException cnse) {
                        // Shouldn't happen; Sqoop-generated classes support clone().
                        throw new IOException(cnse);
                    }
                }
            }

            if (null != maxWidget) {
                context.write(maxWidget, NullWritable.get());
            }
        }
    }





    @Override
    public int run(String[] args) throws Exception {

        //Provides access to configuration parameters.
        Configuration conf = super.getConf();

        //GenericOptionsParser is a utility to parse command line arguments generic to the Hadoop framework.
        GenericOptionsParser optionsParser = new GenericOptionsParser(conf, args);

        String[] remainingArgs = optionsParser.getRemainingArgs();


        if (remainingArgs.length != 2) {
            System.err.printf("Usage: %s [generic options] <input> <output>\n", getClass().getSimpleName());

            //A utility to help run Tools.
            ToolRunner.printGenericCommandUsage(System.err);

            System.exit(2);
        }


        // Create a new Job
        Job job = Job.getInstance(conf, "Max Widget");
        job.setJarByClass(getClass());

        // Specify various job-specific parameters

        // Set the input file path
        FileInputFormat.addInputPath(job, new Path(args[0]));

        // Set the output file path
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        // Set the mapper, reducer and combiner classes
        job.setMapperClass(MaxWidgetMapper.class);

        job.setReducerClass(MaxWidgetReducer.class);

        // Set the type of the key in the output
        job.setOutputKeyClass(Widget.class);

        // Set the type of the value in the output
        job.setOutputValueClass(NullWritable.class);

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        int exitCode = ToolRunner.run(conf, new MaxWidgetId(), args);
        System.exit(exitCode);
    }
}
