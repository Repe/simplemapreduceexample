package at.fhv.hadoop.examples.stats;

import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

import static java.lang.Math.pow;

public final class DescriptiveStatistics {

    private final static Logger LOGGER = LoggerFactory.getLogger(DescriptiveStatistics.class);

    private DescriptiveStatistics() {
        LOGGER.trace("Entering DescriptiveStatistics()");
        LOGGER.trace("Leaving DescriptiveStatistics()");
    }

    public static Mp updateFormula(Mp a, Mp b) {
        LOGGER.trace("Entering updateFormula(a: {}, b:{})", a, b);
        Preconditions.checkArgument(a.order() == b.order(), "Orders of moments not equal");
        if (a.isEmpty() && !b.isEmpty()) {
            return b;
        }

        if (!a.isEmpty() && b.isEmpty()) {
            return a;
        }

        double value;
        int order = a.order();
        if (order >= 2) {
            value = valueUpdateFormulaOrderGeq2(a,b);
        } else {
            value = valueUpdateFormulaOrderLess2(a,b);
        }

        double average = averageUpdateFormula(a.average(), a.size(), b.average(), b.size());
        Mp[] msOfLowerOrder = calculateMsOfLowerOrder(a, b);

        int nA = a.size();
        int nB = b.size();
        int n = nA + nB;
        Mp result = Mp.create(order, value, n, average, msOfLowerOrder);

        LOGGER.trace("Leaving updateFormula(): {}", result);
        return result;
    }

    private static double valueUpdateFormulaOrderLess2(Mp a, Mp b) {
        LOGGER.trace("Entering valueUpdateFormulaOrderLess2(a: {}, b: {})", a, b);
        Preconditions.checkArgument(a.order() < 2, "Order greater or equal than 2 not supported");
        Preconditions.checkArgument(a.isEmpty() == b.isEmpty(), "Union of empty with non empty not supported");

        double mpA = a.value();
        double mpB = b.value();
        double value = mpA + mpB;

        LOGGER.trace("Leaving valueUpdateFormulaOrderLess2(): {}", value);
        return value;
    }

    private static double valueUpdateFormulaOrderGeq2(Mp a, Mp b) {
        LOGGER.trace("Entering valueUpdateFormulaOrderGeq2(a: {}, b: {})", a, b);
        Preconditions.checkArgument(a.order() >= 2, "Order less than 2 not supported");
        Preconditions.checkArgument(a.isEmpty() == b.isEmpty(), "Union of empty with non empty not supported");

        int nA = a.size();
        int nB = b.size();
        int n = nA + nB;
        int order = a.order();

        double deltaBA = delta(a,b);
        double mpA = a.value();
        double mpB = b.value();

        double value = mpA + mpB
                + nA * pow(((-1.0) * nB) / n * deltaBA, order)
                + nB * pow(((1.0) * nA) / n * deltaBA, order);

        for (int k = 1; k <= order - 2; k++) {
            long bk = Statistics.binomialCoefficient(order, k);
            int neededOrder = order - k;
            double partialResult =
                    bk * pow(deltaBA, k) *
                            (a.mpOfLowerOrder(neededOrder).value() * pow(((-1.0) * nB) / n, k) +
                                    b.mpOfLowerOrder(neededOrder).value() * pow(((1.0) * nA) / n, k)
                            );
            value += partialResult;
        }

        LOGGER.trace("Leaving valueUpdateFormulaOrderGeq2(): {}", value);
        return value;
    }

    public static double averageUpdateFormula(double averageA, int nA, double averageB, int nB) {
        LOGGER.trace("Entering averageUpdateFormula(averageA: {}, nA: {}, averageB: {}, nb: {})", averageA, nA, averageB, nB);

        if (nA != 0 && nB == 0) {
            LOGGER.trace("Leaving averageUpdateFormula(): {}", averageA);
            return averageA;
        }

        if (nA == 0 && nB != 0) {
            LOGGER.trace("Leaving averageUpdateFormula(): {}", averageB);
            return averageB;
        }

        int n = nA + nB;
        double average = averageA + ((1.0)*nB)/n * (averageB - averageA);
        LOGGER.trace("Leaving averageUpdateFormula(): {}", average);
        return average;
    }

    //TODO Remove
    private static Mp[] calculateMsOfLowerOrderOld(Mp a, Mp b) {
        LOGGER.trace("Entering calculateMsOfLowerOrder(a: {}, b: {})", a, b);
        int order = a.order;
        Mp[] result = new Mp[order];
        for (int lowerOrder = 0; lowerOrder < order; lowerOrder++) {
            result[lowerOrder] = updateFormula(a.mpOfLowerOrder(lowerOrder), b.mpOfLowerOrder(lowerOrder));
        }
        LOGGER.trace("Leaving calculateMsOfLowerOrder(): {}", result);
        return result;
    }

    public static Mp[] calculateMsOfLowerOrder(Mp a, Mp b) {
        LOGGER.trace("Entering calculateMsOfLowerOrder(a: {}, b: {})");
        int order = a.order;
        Mp[] result = new Mp[order];
        if (order >= 1) {
            int lowerOrder = order - 1;
            Mp mpOfLowerOrder = updateFormula(a.mpOfLowerOrder(lowerOrder), b.mpOfLowerOrder(lowerOrder));
            Mp[] mps = mpOfLowerOrder.msOfLowerOrder();
            for (int i = 0, resultLength = result.length - 1; i < resultLength; i++) {
                result[i] = mps[i];
            }
            result[lowerOrder] = mpOfLowerOrder;
        }
        LOGGER.trace("Leaving calculateMsOfLowerOrder(): {}", Arrays.toString(result));
        return result;
    }

    private static double delta(Mp a, Mp b) {
        double meanA = a.average();
        double meanB = b.average();
        return meanB - meanA;
    }

    static final class Mp {

        private final Logger logger = LoggerFactory.getLogger(Mp.class);

        // Order of the moment
        private final int order;
        // Value
        private final double value;
        // Size of the set
        private final int size;
        // Average
        private final double average;
        // Ms of lower order
        private final Mp[] msOfLowerOrder;

        public static Mp calculate(int order, double[] values, double average) {
            Preconditions.checkArgument(order >= 0, "Order must bee greater or equal than 0");
            Preconditions.checkNotNull(values, "Values null");

            double value = Double.NaN;
            int size = values.length;

            //TODO refactor for order == 0, order == 1, values.length == 0, values.length == 1
            if (values.length > 0) {
                value = 0.0;
                for (int i = 0; i < values.length; i++) {
                    value += Math.pow(values[i] - average, order);
                }
            }

            Mp[] msOfLowerOrder = new Mp[0];
            if (order > 0) {
                msOfLowerOrder = new Mp[order];
                for (int lowerOrder = 0; lowerOrder <= order - 1; lowerOrder++) {
                    msOfLowerOrder[lowerOrder] = calculate(lowerOrder,values,average);
                }
            }
            return create(order, value, size, average, msOfLowerOrder);
        }

        public static Mp create(int order, double value, int size, double average, Mp[] msOfLowerOrder) {
            return validate(new Mp(order, value, size, average, msOfLowerOrder));
        }

        private static Mp validate(Mp mp) {
            mp.check();
            return mp;
        }

        private Mp(int order, double value, int size, double average, Mp[] msOfLowerOrder) {
            logger.trace("Entering Mp(order: {}, value:{})");
            this.order = order;
            this.value = value;
            this.size = size;
            this.average = average;
            this.msOfLowerOrder = msOfLowerOrder;
            logger.trace("Leaving Mp()");
        }

        private void check() {
            logger.trace("Entering check()");
            logger.trace("Leaving check()");
        }

        public Double value() {
            logger.trace("Entering value()");
            Double result = value;
            logger.trace("Leaving value(): {}", result);
            return result;
        }

        public int size() {
            logger.trace("Entering size()");
            int result = size;
            logger.trace("Leaving size(): {}", result);
            return result;
        }

        public boolean isEmpty() {
            logger.trace("Entering isEmpty()");
            boolean result = size == 0;
            logger.trace("Leaving isEmpty(): {}", result);
            return result;
        }

        public double average() {
            logger.trace("Entering average()");
            double result = average;
            logger.trace("Leaving average()");
            return result;
        }

        public Mp[] msOfLowerOrder() {
            logger.trace("Entering msOfLowerOrder()");
            Mp[] result = new Mp[msOfLowerOrder.length];
            for (int i = 0; i < msOfLowerOrder.length; i++) {
                Mp h = msOfLowerOrder[i];
                result[h.order] = create(h.order(), h.value(), h.size(), h.average(), h.msOfLowerOrder());
            }
            logger.trace("Leaving msOfLowerOrder(): {}", Arrays.toString(result));
            return result;
        }

        public int order() {
            logger.trace("Entering order()");
            int result = order;
            logger.trace("Leaving order(): {}", result);
            return result;
        }

        public Mp mpOfLowerOrder(int neededOrder) {
            logger.trace("Entering mpOfLowerOrder(neededOrder: {})", neededOrder);
            Preconditions.checkArgument(neededOrder < order(), "M of order " + neededOrder + "not available");
            Mp result = msOfLowerOrder[neededOrder];
            logger.trace("Leaving mpOfLowerOrder(): {}", result);
            return result;
        }

        @Override
        public String toString() {
            return "Mp{" +
                    "order=" + order +
                    ", value=" + value +
                    ", size=" + size +
                    ", average=" + average +
                    ", msOfLowerOrder=" + Arrays.toString(msOfLowerOrder) +
                    '}';
        }
    }
}
