package at.fhv.hadoop.examples.stats;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public final class Statistics {

    private final static Logger LOGGER = LoggerFactory.getLogger(Statistics.class);

    public static long binomialCoefficient(long n, long k) {
        LOGGER.trace("Entering binomialCoefficient(n: {}, k: {})", n, k);
        long result = 1;

        if (k == 0) {
            return result;
        }

        if (2 * k > n) {
            k = n - k;
        }

        //System.out.println("n: " + n + " k: " + k);

        for (int i = 1; i <= k; i++) {
            result = result * (n - k + i) / i;
        }

        LOGGER.trace("Leaving binomialCoefficient(): {}", result);
        return result;
    }

    public static double average(double[] values) {
        LOGGER.trace("Entering average(values: {})", Arrays.toString(values));
        double sum = 0.0;
        for (int i = 0, valuesLength = values.length; i < valuesLength; i++) {
            sum += values[i];
        }
        double result = sum / values.length;
        LOGGER.trace("Leaving average(): {}", result);
        return result;
    }




}
