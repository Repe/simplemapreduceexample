package at.fhv.hadoop.examples.mean.v1;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class SumCnt implements Writable {

    private int sum;
    private int cnt;

    public SumCnt(int sum, int cnt) {
        this.sum = sum;
        this.cnt = cnt;
    }


    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeInt(sum);
        dataOutput.writeInt(cnt);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        sum = dataInput.readInt();
        cnt = dataInput.readInt();
    }

    public static SumCnt read(DataInput dataInput) throws IOException {
        SumCnt sumCnt = new SumCnt(0,0);
        sumCnt.readFields(dataInput);
        return sumCnt;
    }

    public int sum() {
        return sum;
    }

    public int cnt() {
        return cnt;
    }
}