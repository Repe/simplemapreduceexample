package at.fhv.hadoop.examples.mean.v1;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class ArithmeticMeanCombinerV1 extends Reducer<Text, SumCnt, Text, SumCnt> {

    public void reduce(Text key, Iterable<SumCnt> values, Context context) throws IOException, InterruptedException {
        int sum = 0;
        int cnt = 0;
        for (SumCnt sumCnt : values) {
            sum += sumCnt.sum();
            cnt += sumCnt.cnt();
        }
        context.write(key, new SumCnt(sum,cnt));
    }
}