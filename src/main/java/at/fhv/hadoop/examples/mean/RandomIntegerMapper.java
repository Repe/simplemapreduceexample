package at.fhv.hadoop.examples.mean;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.Random;

public class RandomIntegerMapper extends Mapper<Text, Text, Text, IntWritable> {

    private int numIntegersToWrite;
    private Random random;
    private int min;
    private int max;
    private int bound;
    private long seed;

    @Override
    public void setup(Context context) {
        Configuration conf = context.getConfiguration();
        numIntegersToWrite = conf.getInt("NUMBERS_PER_MAP",0);
        min = conf.getInt("MIN", 0);
        max = conf.getInt("MAX", 0);
        seed = conf.getLong("SEED", 8L);
        bound = max - min;
        random = new Random(seed);
    }


    @Override
    public void map(Text key, Text value, Context context) throws IOException, InterruptedException {
        while (numIntegersToWrite > 0) {
            // generate the random number
            // nextInt is normally exclusive of the top value,
            // so add 1 to make it inclusive
            int randomNumber = random.nextInt(bound + 1) + min;

            // write the generated random number
            context.write(new Text("A"), new IntWritable(randomNumber));

            numIntegersToWrite--;
        }
    }


}
