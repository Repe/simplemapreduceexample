package at.fhv.hadoop.examples.mean.v0;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class ArithmeticMeanReducerV0 extends Reducer<Text, IntWritable, Text, IntWritable> {

    public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        int sum = 0;
        int cnt = 0;
        for (IntWritable val : values) {
            sum += val.get();
            cnt++;
        }
        context.write(key, new IntWritable(sum/cnt));
    }
}
