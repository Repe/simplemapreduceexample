package at.fhv.hadoop.examples.mean.v2;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.HashMap;

/**
 *
 */
public class ArithmethicMeanMapperV2 extends Mapper<Text, Text, Text, IntWritable> {

    private HashMap<Text, Integer> localCombineResults;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        localCombineResults = new HashMap<>();
    }

    @Override
    protected void map(Text key, Text value, Context context) throws IOException, InterruptedException {

    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {

    }
}
