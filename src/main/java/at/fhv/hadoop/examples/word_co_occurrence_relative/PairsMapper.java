package at.fhv.hadoop.examples.word_co_occurrence_relative;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class PairsMapper extends Mapper<LongWritable, Text, TextPair, IntWritable> {

    HashMap<String, Integer> totalCountWi;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        totalCountWi = new HashMap<>();
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        //Split text on whitespaces
        String[] tokens = value.toString().split("\\s");

        for (int i = 0; i < tokens.length; i++) {

            String word1 = tokens[i].replaceAll("[.]","").toLowerCase();

            for (int j = i+1; j < tokens.length; j++) {

                String word2 = tokens[j].toLowerCase();

                boolean endOfSentence = word2.contains(".");

                context.write(new TextPair(word1, word2.replaceAll("[.]", "")), new IntWritable(1));

                Integer totalCount = totalCountWi.getOrDefault(word1, 0);
                totalCountWi.put(word1, totalCount + 1);

                if (endOfSentence) {
                    break;
                }
            }
        }
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        for (Map.Entry<String, Integer> entry : totalCountWi.entrySet()) {
            context.write(new TextPair(entry.getKey(), TextPair.SPECIAL_CHARATER), new IntWritable(entry.getValue()));
        }
    }
}
