package at.fhv.hadoop.examples.word_co_occurrence_relative;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class PairsReducer extends Reducer<TextPair, IntWritable, TextPair, DoubleWritable> {

    private Text currentWi;
    private int totalCountWi;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        currentWi = null;
    }

    @Override
    protected void reduce(TextPair key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {


        if (currentWi == null || currentWi.compareTo(key.first()) != 0) {

            currentWi = key.first();
            totalCountWi = 0;

            for (IntWritable value : values) {
                totalCountWi += value.get();
            }

        } else {

            int totalNumberOfCoOccurrences = 0;

            for (IntWritable value : values) {
                totalNumberOfCoOccurrences += value.get();
            }

            double relativeFrequency = totalNumberOfCoOccurrences / (1.0 * totalCountWi);

            context.write(key, new DoubleWritable(relativeFrequency));
        }
    }
}
