package at.fhv.hadoop.examples.word_co_occurrence_relative;

import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class StripesReducer extends Reducer<Text, MapWritable, Text, MapWritable> {



    @Override
    protected void reduce(Text key, Iterable<MapWritable> values, Context context) throws IOException, InterruptedException {

        // Calculate the total occurrences of w_i
        int totalCountWi = 0;

        // Calculate the total number of occurrences of the pair (w_i, w_j)
        HashMap<Text, Integer> totalNumberOfCoOccurrences = new HashMap<>();

        // For each stripe
        for (MapWritable value : values) {

            // For each w_j in the stripe
            for (Map.Entry<Writable, Writable> entry : value.entrySet()) {

                Text w_j = (Text) entry.getKey();
                Integer count = ((IntWritable)entry.getValue()).get();

                totalCountWi += count;

                Integer totalCount = totalNumberOfCoOccurrences.getOrDefault(w_j, 0);
                totalNumberOfCoOccurrences.put(w_j, totalCount + count);
            }
        }

        //Calculate relative frequencies
        MapWritable stripe = new MapWritable();

        for (Map.Entry<Text, Integer> entry : totalNumberOfCoOccurrences.entrySet()) {

            //Absolute number of coOccurrences
            Integer totalCount = entry.getValue();

            //Marginal
            Integer marginal = totalCountWi;

            double relativeFrequency = totalCount / (1.0 * marginal);


            stripe.put(entry.getKey(), new DoubleWritable(relativeFrequency));
        }

        context.write(key,stripe);
    }
}
