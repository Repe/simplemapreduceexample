package at.fhv.hadoop.examples.word_co_occurrence_relative;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.zookeeper.txn.Txn;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;
import java.util.StringJoiner;

public class TextPair implements WritableComparable {

    public static String SPECIAL_CHARATER = "**";

    private Text first;
    private Text second;

    public TextPair() {
        set(new Text(), new Text());
    }

    public TextPair(String first, String second) {
        set(new Text(first), new Text(second));
    }

    public TextPair(Text first, Text second) {
        set(first, second);
    }

    private void set(Text first, Text second) {
        this.first = first;
        this.second = second;
    }


    @Override
    public void write(DataOutput dataOutput) throws IOException {
        first.write(dataOutput);
        second.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        first.readFields(dataInput);
        second.readFields(dataInput);
    }

    @Override
    public int compareTo(Object o) {
        TextPair other = (TextPair)o;

        int cmp = first.compareTo(other.first);
        if (cmp != 0) {
            return cmp;
        }
        return second.compareTo(other.second);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "TextPair{", "}")
                .add("first=" + first)
                .add("second=" + second)
                .toString();
    }

    public Text first() {
        return this.first;
    }

    public static class KeyComparator extends WritableComparator {

        public KeyComparator() {
            super(TextPair.class, true);
        }

        @Override
        public int compare(WritableComparable a, WritableComparable b) {
            TextPair tp1 = (TextPair)a;
            TextPair tp2 = (TextPair)b;

            int cmp = tp1.first.compareTo(tp2.first);

            if (cmp != 0) {
                return cmp;
            }

            return tp1.second.compareTo(tp2.second);
        }
    }

    public static class FistPartitioner extends Partitioner<TextPair, NullWritable> {
        @Override
        public int getPartition(TextPair textPair, NullWritable nullWritable, int numPartitions) {
            return (textPair.first.hashCode() & Integer.MAX_VALUE) % numPartitions;
        }
    }

    public static class FirstComparator extends WritableComparator {
        @Override
        public int compare(WritableComparable a, WritableComparable b) {
            if (a instanceof TextPair && b instanceof TextPair) {
                return ((TextPair)a).first.compareTo(((TextPair)b).first);
            }
            return super.compare(a,b);
        }
    }




}
