package at.fhv.hadoop.examples.wordcount_in_mapper_combining;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import javax.security.auth.login.Configuration;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class TokenizerMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    private HashMap<Text, Integer> localCount;
    private int count;

    @Override
    public void setup(Context context) throws IOException, InterruptedException {
        localCount = new HashMap<>();
        count = 0;
    }

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        //Split text on whitespaces
        String[] tokens = value.toString().split("\\s");

        for (String s : tokens) {
            Text term = new Text(s.toLowerCase());
            Integer count = localCount.getOrDefault(term, 0);
            localCount.put(term, count + 1);
        }

        if (count++ % 10 == 0) {
            for (Map.Entry<Text, Integer> entry : localCount.entrySet()) {
                context.write(entry.getKey(), new IntWritable(entry.getValue()));
            }
            localCount = new HashMap<>();
        }
    }

    @Override
    public void cleanup(Context context) throws IOException, InterruptedException {
        for (Map.Entry<Text, Integer> entry : localCount.entrySet()) {
            context.write(entry.getKey(), new IntWritable(entry.getValue()));
        }
    }



}
