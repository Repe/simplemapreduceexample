package at.fhv.hadoop.examples.max_temperature_avro;

import org.apache.hadoop.io.Text;

public class NcdcRecordParser {

    private static final int MISSING_TEMPERATURE = 9999;

    private String year;
    private int airTemperature;
    private boolean airTemperatureMalformed;
    private String quality;
    private String stationId;


    public void parse(String record) {

        stationId = record.substring(4, 10) + "-" + record.substring(10, 15);
        year = record.substring(15,19);
        airTemperatureMalformed = false;

        if (record.charAt(87) == '+') {
            airTemperature = Integer.parseInt(record.substring(88,92));
        } else if (record.charAt(87) == '-') {
            airTemperature = Integer.parseInt(record.substring(87,92));
        } else {
            airTemperatureMalformed = true;
        }
        quality = record.substring(92,93);

    }

    public void parse(Text record) {
        parse(record.toString());
    }

    public String year() {
        return year;
    }

    public int airTemperature() {
        return airTemperature;
    }

    public boolean isValidTemperature() {
        return !airTemperatureMalformed && airTemperature != MISSING_TEMPERATURE && quality.matches("[01459]");
    }

    public boolean isMalformedTemperature() {
        return airTemperatureMalformed;
    }

    public boolean isMissingTemperature() {
        return airTemperature == MISSING_TEMPERATURE;
    }

    public String quality() {
        return quality;
    }

    public Integer yearInt() {
        return Integer.parseInt(year);
    }

    public String stationId() {
        return stationId;
    }
}
