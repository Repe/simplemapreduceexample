package at.fhv.hadoop.examples.max_temperature_avro;


import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class NCDCRecordToAvroMapper extends Mapper<LongWritable, Text, AvroKey<Integer>, AvroValue<GenericRecord>> {

    private GenericRecord record = new GenericData.Record(Schemas.INSTANCE.weatherRecordSchema());
    private NcdcRecordParser parser = new NcdcRecordParser();


    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        parser.parse(value);

        if (parser.isValidTemperature()) {
            record.put("year", parser.yearInt());
            record.put("temperature", parser.airTemperature());
            record.put("stationId", parser.stationId());
            System.out.println("Write " + parser.yearInt() + ", " + parser.airTemperature() + ", " + parser.stationId());
            context.write(new AvroKey<>(parser.yearInt()), new AvroValue<>(record));
        }
    }
}
