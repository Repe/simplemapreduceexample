package at.fhv.hadoop.examples.max_temperature_avro;


import org.apache.avro.Schema;

import java.io.IOException;

public enum Schemas {
    INSTANCE;

    private String weatherRecordSchemaFile = "/avro/WeatherRecord.avsc";
    private Schema weatherRecordSchema;

    Schemas() {
        //Create the parser for the schema
        Schema.Parser parser = new Schema.Parser();

        //Create schema from .avsc file
        try {
            weatherRecordSchema = parser.parse(getClass().getResourceAsStream(weatherRecordSchemaFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Schema weatherRecordSchema() {
        return weatherRecordSchema;
    }
}
