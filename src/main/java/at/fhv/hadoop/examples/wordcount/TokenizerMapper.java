package at.fhv.hadoop.examples.wordcount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

public class TokenizerMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        //Split text on whitespaces
        String[] tokens = value.toString().split("\\s");

        for (String s : tokens) {
            context.write(new Text(s.toLowerCase()), new IntWritable(1));
        }
    }
}
