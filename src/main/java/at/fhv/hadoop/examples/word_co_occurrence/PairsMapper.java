package at.fhv.hadoop.examples.word_co_occurrence;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 *
 */
public class PairsMapper extends Mapper<LongWritable, Text, TextPair, IntWritable> {

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        //Split text on whitespaces
        String[] tokens = value.toString().split("\\s");

        for (int i = 0; i < tokens.length; i++) {

            String word1 = tokens[i].replaceAll("[.]","").toLowerCase();

            for (int j = i+1; j < tokens.length; j++) {

                String word2 = tokens[j].toLowerCase();

                boolean endOfSentence = word2.contains(".");

                context.write(
                        new TextPair(word1, word2.replaceAll("[.]", "")),
                        new IntWritable(1));

                if (endOfSentence) {
                    break;
                }
            }
        }
    }
}
