package at.fhv.hadoop.examples.word_co_occurrence;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Map;

public class StripesReducer extends Reducer<Text, MapWritable, Text, MapWritable> {

    @Override
    protected void reduce(Text key, Iterable<MapWritable> values, Context context) throws IOException, InterruptedException {

        MapWritable stripe = new MapWritable();

        for (MapWritable value : values) {
            for (Map.Entry<Writable, Writable> entry : value.entrySet()) {

                Text entryKey = (Text)entry.getKey();
                IntWritable count = (IntWritable)entry.getValue();

                IntWritable totalCount = stripe.containsKey(entryKey) ? (IntWritable)stripe.get(entryKey) : new IntWritable(0);

                stripe.put(entryKey, new IntWritable(totalCount.get() + count.get()));
            }
        }
        context.write(key, stripe);
    }
}
