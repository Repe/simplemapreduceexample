package at.fhv.hadoop.examples.word_co_occurrence;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.StringJoiner;

public class TextPair implements WritableComparable {

    private Text first;
    private Text second;

    public TextPair() {
        set(new Text(), new Text());
    }

    public TextPair(String first, String second) {
        set(new Text(first), new Text(second));
    }

    public TextPair(Text first, Text second) {
        set(first, second);
    }

    private void set(Text first, Text second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        first.write(dataOutput);
        second.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        first.readFields(dataInput);
        second.readFields(dataInput);
    }

    @Override
    public int compareTo(Object o) {
        TextPair other = (TextPair)o;

        int cmp = first.compareTo(other.first);
        if (cmp != 0) {
            return cmp;
        }
        return second.compareTo(other.second);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "TextPair{", "}")
                .add("first=" + first)
                .add("second=" + second)
                .toString();
    }
}
