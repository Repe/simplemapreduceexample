package at.fhv.hadoop.examples.word_co_occurrence;

import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class StripesMapper extends Mapper<LongWritable, Text, Text, MapWritable> {

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        //Split text on whitespaces
        String[] tokens = value.toString().split("\\s");

        for (int i = 0; i < tokens.length; i++) {

            String word1 = tokens[i].replaceAll("[.]","").toLowerCase();

            MapWritable stripe = new MapWritable();

            for (int j = i+1; j < tokens.length; j++) {

                String word2 = tokens[j].toLowerCase();

                boolean endOfSentence = word2.contains(".");

                Text word2AsText = new Text(word2.replaceAll("[.]",""));

                IntWritable count = stripe.containsKey(word2AsText) ? (IntWritable)stripe.get(word2AsText) : new IntWritable(0);

                stripe.put(word2AsText, new IntWritable(count.get() +1));

                if (endOfSentence) {
                    break;
                }
            }
            context.write(new Text(word1), stripe);
        }
    }
}
