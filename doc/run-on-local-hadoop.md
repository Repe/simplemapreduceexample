# Start
start-dfs.sh
start-yarn.sh

# Web

Browse the web interface for the NameNode; by default it is available at:
NameNode - http://localhost:9870/

Browse the web interface for the ResourceManager; by default it is available at:
ResourceManager - http://localhost:8088/

# Run WordCount on Local Hadoop

scp -P 22 .\hadoop-simple-examples.jar hadoop@hadoop-cluster:~
hadoop jar hadoop-simple-examples.jar at.fhv.hadoop.examples.WordCountDriver input/wordcount output/wordcount

# Run MaxTemperatureAvro
## Prepare

Download `avro-1.10.2.jar` and `avro-mapred-1.10.2.jar` to `/usr/local/hadoop/share/hadoop/common/lib`.

* `cd /usr/local/hadoop/share/hadoop/common/lib`
* `wget https://downloads.apache.org/avro/avro-1.10.2/java/avro-1.10.2.jar`
* `wget https://downloads.apache.org/avro/avro-1.10.2/java/avro-mapred-1.10.2-sources.jar`

## Run
hadoop jar hadoop-simple-examples.jar at.fhv.hadoop.examples.MaxTemperatureAvroDriver input/ncdc_weather/all output/ncdc

## View Results
hadoop fs -get output/ncdc/part-r-00000.avro res.avro

wget https://dlcdn.apache.org/avro/avro-1.10.2/java/avro-tools-1.10.2.jar
java -jar avro-tools-1.10.2.jar getschema res.avro
java -jar avro-tools-1.10.2.jar tojson res.avro

### Hints

<http://www.mtitek.com/tutorials/bigdata/hadoop/avro-tools.php>







